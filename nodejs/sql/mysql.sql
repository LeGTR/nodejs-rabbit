-- Create the table in the specified schema
CREATE TABLE CALLBACK
(
    callback_id INT NOT NULL PRIMARY KEY, -- primary key column
    callback json NOT NULL,
    type varchar(255) NOT NULL,
    attempt varchar(255) NOT NULL,
    status int(2)
    -- specify more columns here
);

-- ALTER TABLE CALLBACK CHANGE callback_id callback_id int NOT NULL AUTO_INCREMENT FIRST;
