-- Create the table in the specified schema
CREATE TABLE CALLBACK
(
    callback_id INT NOT NULL PRIMARY KEY, -- primary key column
    callback json NOT NULL,
    type varchar NOT NULL,
    attempt varchar NOT NULL,
    status int
    -- specify more columns here
);
