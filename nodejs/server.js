// Import packages and set the port

const port = 3002;
const routes = require("./routes/routes.js");
const express = require("express");

const app = express();

// Use Node.js body parsing middleware
app.use(express.json());

app.use('/api', routes)
// Start the server
const server = app.listen(port, (error) => {
	if (error) return console.log(`Error: ${error}`);
	console.log(`Server listening on port ${server.address().port}`);
});