
module.exports = class CallbackModel{
 
    //mongo = require("../db/mongo.js");
    mysql = require("../db/mysql.js");
    pg = require("../db/pg.js");
    
    constructor(
        callback_id = '',
        callback = '',
        type = '',
        attempt = '',
        status = ''
    ){

        this.callback_id = callback_id;
        this.callback = callback;
        this.type = type;
        this.attempt = attempt;
        this.status = status;
        
    }
    save(){
        var sql = '';
        if ( this.callback_id == '' ) {
            sql = "INSERT INTO CALLBACK (callback, attempt, status, type) VALUES ("
                + "'" + this.callback + "'"
                + "," + this.attempt
                + "," + this.status
                + "," + "'" + this.type + "'" + ");";
        }else{
            sql = "INSERT INTO CALLBACK (callback, attempt, status, type) VALUES ("
                + "'" + this.callback + "'"
                + "," + this.attempt
                + "," + this.status
                + "," + "'" + this.type + "'" + ") WHERE callback_id = '" + this.callback_id + "';";
        }
        this.query_db( sql );
    }

    delete( callback_id = ''){
        if ( callback_id == '' ) {
            return false;
        }
        sql = "DELETE FROM CALLBACK WHERE callback_id = '" + callback_id + "';";
        this.query_db( sql );
    }


    query_db( sql ){
        console.log( sql );
        var _mysql = '';
        var _pg = '';
        _mysql = this.mysql.query( sql );
        _pg = this.pg.query( sql );

        return {"mysql" : _mysql, "pg" : _pg};
    }

    static getCallback(callback_id = ''){
        if ( callback_id == '' ) {
            return false;
        }

        sql = "SELECT * FROM CALLBACK WHERE callback_id = '" + callback_id + "';";
        result = this.query_db( sql );

        return result;
    }
    
    static getAll(){
        sql = "SELECT * FROM CALLBACK ;";
        result = this.query_db( sql );

        return result;
    }
}

