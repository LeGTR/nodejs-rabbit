
const { MongoClient } = require('mongodb');
// or as an es module:
// import { MongoClient } from 'mongodb'

// Connection URL
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
// Database Name
const dbName = 'nodejs';

await client.connect();
console.log('Connected successfully to server');
const mongo = client.db(dbName);

module.exports = mongo;