const amqplib = require('amqplib');
const amqpUrl = process.env.AMQP_URL || 'amqp://localhost:5673';

class ApiController {
    async add(req, res){
        console.log(req);
        res.send("test add");
    }
    async ampq_test(req, res){
        const connection = await amqplib.connect(amqpUrl, 'heartbeat=60');
        const channel = await connection.createChannel();

        const {id, name, email} = req.body
        console.log('Publishing');
        const exchange = 'user.signed_up';
        const queue = 'user.sign_up_email';
        const routingKey = 'sign_up_email';
        
        await channel.assertExchange(exchange, 'direct', {durable: true});
        await channel.assertQueue(queue, {durable: true});
        await channel.bindQueue(queue, exchange, routingKey);
        
        const msg = {'id': id, 'email': email, 'name': name};
        //const msg = {'id': "1", 'email': "test@test.ru", 'name': "lastname first"};
        await channel.publish(exchange, routingKey, Buffer.from(JSON.stringify(msg)));
        console.log('Message published');
        res.send("amqplib add");
    }
}

module.exports = new ApiController();